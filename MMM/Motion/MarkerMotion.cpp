
#include "MarkerMotion.h"

namespace MMM
{

MarkerMotion::MarkerMotion(unsigned int frames)
{
	setNumFrames(frames);
}

unsigned int MarkerMotion::getNumFrames()
{
	return (unsigned int)markerTrajectory.size();
}

size_t MarkerMotion::getNumMarkers()
{
	return markerLabels.size();
}

void MarkerMotion::setNumFrames(size_t frames)
{
	if (frames==0)
	{
		markerTrajectory.clear();
		return;
	}
	markerTrajectory.resize(frames);

	// init data structures
	for (unsigned int i=0;i<frames;i++)
	{
		markerTrajectory[i].reset(new MarkerData());
	}
}

std::string MarkerMotion::getMarkerLabel(size_t i)
{
	if (i>=markerLabels.size())
	{
		MMM_ERROR << "MarkerTrajectory OutOfBounds Error, Marker " << i << endl;
		return std::string();
	}
	return markerLabels[i];
}

void MarkerMotion::setMarkerLabels(std::vector< std::string > &labels)
{
	markerLabels = labels;
}

std::vector< std::string > MarkerMotion::getMarkerLabels()
{
	return markerLabels;
}

MarkerDataPtr MarkerMotion::getFrame(size_t frame)
{
	if (frame>=markerTrajectory.size())
	{
		MMM_ERROR << "MarkerTrajectory OutOfBounds Error, Frame " << frame << endl;
		return MarkerDataPtr();
	}
	return markerTrajectory[frame];
}

bool MarkerMotion::hasMarkerLabel(const std::string &labelName) const
{
	return (std::find(markerLabels.begin(),markerLabels.end(),labelName) != markerLabels.end());
}

void MarkerMotion::setFrame(size_t frame, MarkerDataPtr data)
{
	if (frame>=markerTrajectory.size())
	{
		MMM_ERROR << "MarkerTrajectory OutOfBounds Error, Frame " << frame << endl;
		return;
	}
	if (!data)
	{
		MMM_ERROR << "MarkerTrajectory Null data Error" << endl;
		return;
	}
	markerTrajectory[frame] = data;
}

void MarkerMotion::appendFrame(MarkerDataPtr data)
{
	if (!data)
	{
		MMM_ERROR << "MarkerTrajectory Null data Error" << endl;
		return;
	}
	markerTrajectory.push_back(data);
}

void MarkerMotion::print()
{
	cout << "---------- MarkerMotion ----------------" << endl;
	cout << "Labels:" << endl;
	for (size_t i=0;i<markerLabels.size();i++)
		cout << " * " << i << ":" << markerLabels[i] << endl;
	cout << "MotionFrame:" << endl;
	for (size_t i=0;i<markerTrajectory.size();i++)
	{
		cout << " *** " << i << ":" << endl;
		markerTrajectory[i]->print();
	}
	cout << "---------- MarkerMotion ----------------" << endl;
}

std::string MarkerMotion::toXML()
{
	std::string tab = "\t";
	std::stringstream res;
	//res << "<? xml version='1.0' ?>" << endl;

	std::map<std::string, MotionEntryPtr>::iterator i = motionEntries.begin();
	while (i != motionEntries.end())
	{
		res << i->second->toXML();
		i++;
	}

	if (markerLabels.size()>0)
	{

		res << "<labels>" << endl;
		for (size_t i=0;i<markerLabels.size();i++)
		{
			res << tab << "<name>" << markerLabels[i] << "</name>" << endl;
		}
		res << "</labels>" << endl;
	}

	if (markerTrajectory.size()>0)
	{
		res << "<marker-motion>" << endl;
		for (size_t i=0;i<markerTrajectory.size();i++)
		{
			res << markerTrajectory[i]->toXML(); 
		}
		res << "</marker-motion>" << endl;
	}

	return res.str();
}


}
